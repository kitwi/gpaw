The PAW calculator object
=========================

.. autoclass:: gpaw.aseinterface.GPAW
   :members:
   :inherited-members:

.. autoclass:: gpaw.paw.PAW
   :members:
   :inherited-members:
